### Author

@This Plugin developed by Md. Rashiduzzaman
@Author Name 	: Rashid
@Author  URL 	: http://portfolio.linkinzone.com/;
@Creating Date 	: 16-DEC-2017
@©copyright www.linkinzone.com



# RzCalculator

Touch enabled [jQuery](https://jquery.com/) plugin that lets you create a beautiful, responsive calculator. **To get started, check out https://services.linkinzone.com/rzcalc/.**

## Quick start

### Install

This package can be installed with:

download the [latest release](https://gitlab.com/rashid/rzcalc).

### Load

#### Static HTML

Put the required stylesheet at the [top](https://developer.yahoo.com/performance/rules.html#css_top) of your markup:

```html
<link rel="stylesheet" href="/css/rzcalc.css">
```
OR
```html
<link rel="stylesheet" href="/css/rzcalc.min.css">
```

Put the script at the [bottom](https://developer.yahoo.com/performance/rules.html#js_bottom) of your markup right after jQuery:

```html
<script src="/js/jquery.js"></script>
```
```html
<script src="/js/rzCalc.js"></script>
```
OR
```html
<script src="/js/rzCalc.min.js"></script>
```

### Usage

Wrap your items (`section`,`div`,`span` etc.). Only no class or id is mandatory to apply proper styles:

```html
<div></div>
```
```html
<div id="myId"></div>
```
OR
```html
<div class="myClass"></div>
```
OR
```html
<div id="myId" class="myClass"></div>
```

Call the [plugin](https://learn.jquery.com/plugins/) function and your calculator is ready.

```javascript
$(document).ready(function(){
  $('div').rzCalc();
});
```
```javascript
$(document).ready(function(){
  $('#myClass').rzCalc();
});
```
OR
```javascript
$(document).ready(function(){
  $('.myClass').rzCalc();
});
```

## Documentation

The documentation, included in this repo in the root directory, is publicly available at [Linkinzone](http://services.linkinzone.com/rzcalc). The documentation may also be run locally.

## License

The code and the documentation are released under the [LINKINZONE License](LICENSE).
